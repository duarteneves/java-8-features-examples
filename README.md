**Java 8 Feature & Exemplos**

Projecto com exemplos de features Java 8, com incid�ncia na nova defini��o de interfaces, uso de lambdas e streams, incluindo Optional, uso de Nashown e manuseamento de datas / tempos recorrendo a API.

Os exemplos s�o sempre acompanhados de solu��es antes de Java 8 e com Java 8.

No �mbito de cada classe representativa dos t�picos j� identificados, pode estar presente um breve coment�rio das vantagens e objetivos da respetiva feature.

Cada package � representativo das features identifcadas no 1� par�grafos, existindo pelo menos uma classe onde � disponiblizada o t�pico m�todo main, ao despoletar a sua execu��o ser�o impressos os resultados dos m�todos definidos sem e com API Java 8.

Este projeto serve de sensibliza��o para uso efetivo da API Java 8.

---

## Clone do Projecto, importar no Eclipse e execu��o

1. Fazer clone do projecto a partir de https://bitbucket.org/duarteneves/java-8-features-examples
2. Se usar Eclipse importar como File -> Import -> Existing Projects into workspace
3. Compilar com Java 8
4. Executar como Java Application as seguintes classes: TaxContribuinteColetivo, TaxContribuinteSingular, Lambda, Nashorn, Optional e Time

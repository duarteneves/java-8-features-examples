package java8.newfeatures.examples.interfaces;

/**
 * Antes Java 8:
 * 		obrigatório adicionar todos os métodos na classe que a implementa, alternativa, temos as classes abstratas
 * 		impacto na atualização da API de uma interfaace, por exemplo interface List
 * 
 * Com Java 8:
 * 		declaração de variáveis: public, final & static
 * 		implemenação de métodos estáticos (static)
 * 		implementação de métodos default
 *
 */
public interface Tax {

	static final int YEAR = 2018;
	
	static final double IVA = 0.23;
	
	default boolean hasIRS() {
		return true;
	}

	default boolean hasIRC() {
		return true;
	}

	default boolean hasIMI() {
		return true;
	}

	default boolean hasIVA() {
		return true;
	}
	
	
	/**
	 * MAIN
	 */
	public static void main(String[] args) {
		
		Tax user = new TaxContribuinteColetivo();
		
		System.out.println("[TaxContribuinteColetivo]");
		
		System.out.println("YEAR: " + YEAR);

		System.out.println("hasIRC: " + user.hasIRC());
		System.out.println("hasIRS: " + user.hasIRS());
		
		
		user = new TaxContribuinteSingular();
		
		System.out.println("[TaxContribuinteSingular]");
		
		System.out.println("YEAR: " + YEAR);

		System.out.println("hasIRC: " + user.hasIRC());
		System.out.println("hasIRS: " + user.hasIRS());		

	}	
	
}

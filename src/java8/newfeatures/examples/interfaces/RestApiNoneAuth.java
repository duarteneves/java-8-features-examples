package java8.newfeatures.examples.interfaces;

public class RestApiNoneAuth implements RestApiAuth {

	@Override
	public void process() {
		System.out.println(RESTAPI_NONE_AUTH);
	}
	
}

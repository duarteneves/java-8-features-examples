package java8.newfeatures.examples.interfaces;

public interface RestApiAuth {
	
	public static final String RESTAPI_NONE_AUTH = "restApiNoneAuth";
	public static final String RESTAPI_BASIC_AUTH = "restApiBasicAuth";

	public static RestApiAuth getAuthMethod(String authMethod) {
		
		if(authMethod == null || authMethod.isEmpty()) {
			return new RestApiNoneAuth();
		}
		
		switch (authMethod) {
		
			case RESTAPI_NONE_AUTH:
				return new RestApiNoneAuth();
			case RESTAPI_BASIC_AUTH:
				return new RestApiBasicAuth();
			default:
				return new RestApiNoneAuth();
				
		}
		
	}
	
	public void process();
	
	
	/**
	 * MAIN
	 */
	public static void main(String[] args) {
		
		// assumir leitura de parametro definido ficheiro de propriedades, por exemplo
		
		String authMethod = RESTAPI_NONE_AUTH;
		
		RestApiAuth bean = RestApiAuth.getAuthMethod(authMethod);
		
		bean.process();

		
		// assumir leitura de parametro definido ficheiro de propriedades, por exemplo

		authMethod = null;
		
		bean = RestApiAuth.getAuthMethod(authMethod);
		
		bean.process();

		
		// assumir leitura de parametro definido ficheiro de propriedades, por exemplo

		authMethod = "";
		
		bean = RestApiAuth.getAuthMethod(authMethod);
		
		bean.process();
		

		// assumir leitura de parametro definido ficheiro de propriedades, por exemplo
		
		authMethod = RESTAPI_BASIC_AUTH;
		
		bean = RestApiAuth.getAuthMethod(authMethod);
		
		bean.process();
		
	}

	
}

package java8.newfeatures.examples.optional;

public class UserOptional {

	private String name;
	private long nif;
	
	private java.util.Optional<ProfileOptional> profile;

	public UserOptional(String name, long nif, java.util.Optional<ProfileOptional> profile) {
		this.name = name;
		this.nif = nif;
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public long getNif() {
		return nif;
	}

	public java.util.Optional<ProfileOptional> getProfile() {
		return profile;
	}
	
}

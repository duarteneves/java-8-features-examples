package java8.newfeatures.examples.optional;

/**
 * Vantagens Optional:
 * 		Diminuir verifica��o do j� comum != null e simplificar c�digo
 * 		Diminuir NPE em runtime 
 *
 */
public class Optional {
	
	private static final String PERMISSION_IS_NOT_DEFINED = "Permission is not defined!";

	private static void printUserPermissionWitoutOptional(User user) {
		
		System.out.println("[printUserPermissionWitoutOptional] Imprimir permiss�o do utilizador: ");
		
		if(user != null) {
			
			Profile profile = user.getProfile();
			
			if(profile != null) {
				
				Permission permission = profile.getPermission();
				
				if(permission != null) {
					
					System.out.println("Permission: " + permission.getName());
					
				} else {
					
					System.out.println("Permission: " + PERMISSION_IS_NOT_DEFINED);
				}
				
			} else {
				
				System.out.println("Permission: " + PERMISSION_IS_NOT_DEFINED);
			}
			
		} else {

			System.out.println("Permission: " + PERMISSION_IS_NOT_DEFINED);
			
		}
		
	}

	private static void printUserPermissionWithOptional(java.util.Optional<UserOptional> user) {
		
		System.out.println("[printUserPermissionWithOptional] Imprimir permiss�o do utilizador: ");
		
		String permission = user.flatMap(UserOptional::getProfile)
				.flatMap(ProfileOptional::getPermission)
				.map(Permission::getName).orElse(PERMISSION_IS_NOT_DEFINED);
		
		System.out.println("Permission: " + permission);		
		
	}

	public static void main(String[] args) {
		
		/**
		 * Init objects
		 * */
		
		Permission permission = new Permission("Update");
		
		Profile profile = new Profile("President", "P", permission);
		
		User user = new User("Francisco Rodrigues", 240623827, profile);

		ProfileOptional profileOptional = new ProfileOptional("President", "P", 
				java.util.Optional.of(permission));

		UserOptional userOptional = new UserOptional("Francisco Rodrigues", 240623827, 
				java.util.Optional.of(profileOptional));
		
		
		/**
		 * Calling methods
		 * */
		
		printUserPermissionWitoutOptional(user);

		printUserPermissionWitoutOptional(null);
		
		printUserPermissionWithOptional(java.util.Optional.of(userOptional));	
		
		printUserPermissionWithOptional(java.util.Optional.ofNullable(null));

		try {
			
			printUserPermissionWithOptional(java.util.Optional.of(null));		
			
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
		
	}
	
}

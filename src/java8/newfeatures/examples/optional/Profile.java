package java8.newfeatures.examples.optional;

public class Profile {

	private String name;
	private String code;

	private Permission permission;

	public Profile(String name, String code, Permission permission) {
		this.name = name;
		this.code = code;
		this.permission = permission;
	}
	
	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public Permission getPermission() {
		return permission;
	}

}

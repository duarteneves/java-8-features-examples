package java8.newfeatures.examples.optional;

public class Permission {

	private String name;
	
	public Permission(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}

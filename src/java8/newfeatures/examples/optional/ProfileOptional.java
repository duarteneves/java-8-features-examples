package java8.newfeatures.examples.optional;

import java.util.Optional;

public class ProfileOptional {

	private String name;
	private String code;

	private Optional<Permission> permission;

	public ProfileOptional(String name, String code, Optional<Permission> permission) {
		this.name = name;
		this.code = code;
		this.permission = permission;
	}
	
	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public Optional<Permission> getPermission() {
		return permission;
	}

}

package java8.newfeatures.examples.optional;

public class User {

	private String name;
	private long nif;
	
	private Profile profile;

	public User(String name, long nif, Profile profile) {
		this.name = name;
		this.nif = nif;
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public long getNif() {
		return nif;
	}

	public Profile getProfile() {
		return profile;
	}
	
}

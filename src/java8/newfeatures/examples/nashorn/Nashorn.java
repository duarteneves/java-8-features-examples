package java8.newfeatures.examples.nashorn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * JAVA <-> Javascript
 * 
 * Validações Locais & Validações Servidor
 * 
 * With plans to deprecate the Nashorn JavaScript engine in the upcoming Java
 * Development Kit (JDK) 11, Oracle is encouraging developers to take a look at
 * GraalVM virtual machine instead
 * 
 */
@Deprecated
public class Nashorn {
	
	private static ScriptEngine engine;
	
	static {
		
		try {

			engine = new ScriptEngineManager().getEngineByName("nashorn");
			
			URL url = Nashorn.class.getResource("validations.js");
			File file = new File(url.getPath());
			
			engine.eval(new FileReader(file));
		
		} catch (FileNotFoundException | ScriptException e) {
			// do nothing
			// don't do that in real world
		}
		
	}

	public static boolean isValidNif(long nif) {

		try {
			
			Object result = ((Invocable) engine).invokeFunction("isValidNif", nif);
			
			return Boolean.valueOf(result.toString());
			
		} catch (NoSuchMethodException | ScriptException e) {
			// do nothing
			// don't do that in real world
		}
		
		return false;
		
	}

	public static boolean isValidPhone(long phone) {
		
		try {
			
			Object result = ((Invocable) engine).invokeFunction("isValidPhone", phone);
			
			return Boolean.valueOf(result.toString());
			
		} catch (NoSuchMethodException | ScriptException e) {
			// do nothing
			// don't do that in real world
		}
		
		return false;
		
	}

	public static boolean isValidEmail(String email) {
		
		try {
			
			Object result = ((Invocable) engine).invokeFunction("isValidEmail", email);
			
			return Boolean.valueOf(result.toString());
			
		} catch (NoSuchMethodException | ScriptException e) {
			// do nothing
			// don't do that in real world
		}
		
		return false;
		
	}

	public static boolean isValidCC(long cc) {
		System.out.println("isValidCC cc: " + cc);
		return true;
	}

	private static boolean isValidCC_JS(long cc) {
		
		try {
			
			Object result = ((Invocable) engine).invokeFunction("isValidCC", cc);
			
			return Boolean.valueOf(result.toString());
			
		} catch (NoSuchMethodException | ScriptException e) {
			// do nothing
			// don't do that in real world
		}
		
		return false;
		
	}
	
	public static void main(String[] args) {
		
		System.out.println("[isValidNif(-1)]: " + isValidNif(-1));
		System.out.println("[isValidNif(123456789)]: " + isValidNif(123456789));
		
		System.out.println("[isValidPhone(123)]: " + isValidPhone(123));
		System.out.println("[isValidPhone(962949476)]: " + isValidPhone(962949476));
		
		System.out.println("[isValidEmail(dneves.pt)]: " + isValidEmail("dneves.pt"));
		System.out.println("[isValidEmail(dneves@opensoft.pt)]: " + isValidEmail("dneves@opensoft.pt"));

		System.out.println("[isValidCC_JS(123456789)]: " + isValidCC_JS(123456789));
		System.out.println("[isValidCC_JS(100100112)]: " + isValidCC_JS(100100112));
		
	}
	
}


var isValidNif = function(nif) {
	
	var pattern = /^\d{9}$/;
	
	return pattern.test(String(nif));
	
};

var isValidPhone = function (phone) {
	
	var pattern = /^\d{9}$/;
	
	return pattern.test(String(phone));
	
};

var isValidEmail = function (email) {
	
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    return pattern.test(String(email).toLowerCase());
	
};

var isValidCC = function (cc) {
	
	var NashornClass = Java.type('java8.newfeatures.examples.nashorn.Nashorn');

	return NashornClass.isValidCC(cc);
	
}

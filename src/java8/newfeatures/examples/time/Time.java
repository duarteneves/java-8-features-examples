package java8.newfeatures.examples.time;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Melhorias API
 *
 * Instant � represents a point in time (timestamp)
 * LocalDate � represents a date (year, month, day)
 * LocalDateTime � same as LocalDate, but includes time with nanosecond precision
 * OffsetDateTime � same as LocalDateTime, but with time zone offset
 * LocalTime � time with nanosecond precision and without date information
 * ZonedDateTime � same as OffsetDateTime, but includes a time zone ID
 * OffsetLocalTime � same as LocalTime, but with time zone offset
 * MonthDay � month and day, without year or time
 * YearMonth � month and year, without day or time
 * Duration � amount of time represented in seconds, minutes and hours. Has nanosecond precision
 * Period � amount of time represented in days, months and years
 *
 */
public class Time {

	private static void printCurrentTime() {

		// Old
		Date nowOld = new Date();

		System.out.println(nowOld.toString());

		// New
		ZonedDateTime nowNew = ZonedDateTime.now();

		System.out.println(nowNew.toString());

	}

	private static void printSpecificTime(int year, int month, int day) {

		// Old
		Date birthDayOld = new GregorianCalendar(year, month, 15).getTime();

		System.out.println(birthDayOld.toString());

		// New
		LocalDate birthDayNew = LocalDate.of(year, month, 15);

		System.out.println(birthDayNew.toString());

	}

	private static void addTime() {

		// Old
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, 2);
		
		Date resultOld = calendar.getTime();

		System.out.println(resultOld.toString());

		// New
		LocalDateTime resultNew = LocalDateTime.now().plusHours(2);

		System.out.println(resultNew.toString());

	}
	
	private static void calcElapsedTime() {
		
		// Old
		
		long start = System.currentTimeMillis();
		
		try {
			
			TimeUnit.SECONDS.sleep(2);
			
		} catch (InterruptedException e) {
			// do nothing
			// don't do that in real world
		}
		
		long finish = System.currentTimeMillis();
		
		System.out.println("Elapsed Time: " + (finish - start));

		
		// New
		
		Instant instanceStart = Instant.now();

		try {
			
			TimeUnit.SECONDS.sleep(2);
			
		} catch (InterruptedException e) {
			// do nothing
			// don't do that in real world
		}
		
		Instant instanceFinish = Instant.now();
		
		
		Duration elapsedTime = Duration.between(instanceStart, instanceFinish);

		System.out.println("Elapsed Time (mili segundos): " + elapsedTime.toMillis());

		System.out.println("Elapsed Time (segundos): " + elapsedTime.toMillis() / 1000);

		System.out.println("Elapsed Time (minutos): " + elapsedTime.toMinutes());
		
	} 

	public static void main(String[] args) {

		printCurrentTime();

		// verificar que a a partir do Java o m�s � mesmo Setembro, n�o � necess�rio estar
		// com +1 como na API anterior
		
		printSpecificTime(2018, Month.SEPTEMBER.getValue(), 26);

		addTime();
		
		calcElapsedTime();
		
	}

}

package java8.newfeatures.examples.trycatch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TryCatch {

	/**
	 * Pre Java 8
	 * */
	
	public static void trycatchPreJava8() {
		
		Scanner scanner = null;

		System.out.println("trycatchBeforeJava8");
		
		try {
			
			scanner = new Scanner(new File("resources/meetup.txt"));
		    
		    while (scanner.hasNext()) {
		        System.out.println(scanner.nextLine());
		    }
		    
		} catch (Exception e) {
		    // don't do that
		} finally {
			
		    if (scanner != null) {
		        scanner.close();
		    }

		}
		
	}
	
	/**
	 * Java 8
	 * */
	
	public static void tryCatch() {
		
		System.out.println("tryCatch");
		
		try (Scanner scanner = new Scanner(new File("resources/meetup.txt"))) {
			
		    while (scanner.hasNext()) {
		        System.out.println(scanner.nextLine());
		    }
		    
		} catch (FileNotFoundException e) {
			// don't do that
		}
		
		
		/**
		 * Multiple resources
		 * */
		
		System.out.println("Copy read to write!");
		
		try (
				Scanner scanner = new Scanner(new File("resources/read.txt"));
				PrintWriter writer = new PrintWriter(new File("resources/write.txt"))
						
			) {
			
			while (scanner.hasNext()) {
				
				String line = scanner.nextLine();
				
				System.out.println(line);
				
				writer.print(line);
				
			}
			
		} catch (FileNotFoundException e) {
			// don't do that
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		
		trycatchPreJava8();
		
		tryCatch();
		
	}
	
}

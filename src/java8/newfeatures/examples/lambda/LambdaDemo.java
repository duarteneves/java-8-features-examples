package java8.newfeatures.examples.lambda;

/**
 * Vantagem de uso de lambdas: 
 * 		diminuir a quantidade de c�digo 
 * 
 * lambda � uma fun��o sem declara��o, n�o � necess�rio colocar um nome, um tipo de retorno e o modificador de acesso.
 * 
 * um m�todo lambda � composto (sintaxe): argumento -> corpo
 *
 */
public class LambdaDemo {

	/**
	 * Pre Java 8
	 * */
	
	public void print(String object) {
		System.out.println(object);
	}

	
	/**
	 * Java 8
	 * */
	
	interface Printer {
		void print(String val);
	}

	public void print(String object, Printer printer) {
		printer.print(object);
	}

	
	/**
	 * MAIN
	 */
	public static void main(String[] args) {

		
		/**
		 * Java OOP (Oriented Object Programming)
		 * */
		
		LambdaDemo demo = new LambdaDemo();
		String value = "Pre Java 8 - Java OOP";
		demo.print(value);

		
		/**
		 * Java FP (Functional Programming)
		 * */
		
		demo = new LambdaDemo();
		value = "Java 8 - Java FP";
		
		demo.print(value, toPrint -> System.out.println(toPrint));
		
		demo.print(value, toPrint -> System.out.println("To be printed: " + toPrint));
		
		demo.print(value, toPrint -> {
			for (int index = 1; index < 5; index++) {
				System.out.println(toPrint + " " + index);
			}
		});

	}

}

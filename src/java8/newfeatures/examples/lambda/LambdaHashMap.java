package java8.newfeatures.examples.lambda;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LambdaHashMap {

	private static void printKeysValuesWithoutLambda() {
	
		System.out.println("printKeysValuesWithoutLambda");
		
		Map<String, User> users = new HashMap<>();

		users.put("224878824", new User("Am�rico da Silva", 224878824));
		users.put("123456789", new User("Alexandre Safara", 123456789));
		users.put("100100112", new User("Luis Salm�o", 100100112));
		users.put("213546469", new User("Duarte Neves", 213546469));
		
		Set<Map.Entry<String, User>> entries = users.entrySet();

		for (Map.Entry<String, User> entry : entries) {
			
			System.out.print("key: " + entry.getKey());
			
			User value = entry.getValue();
			System.out.println(", Value: " + value.getName() + " " + value.getNif());
			
		}
		
	}

	private static void printKeysValuesWithLambda() {
		
		System.out.println("printKeysValuesWithLambda");
		
		Map<String, User> users = new HashMap<>();
		
		users.put("224878824", new User("Am�rico da Silva", 224878824));
		users.put("123456789", new User("Alexandre Safara", 123456789));
		users.put("100100112", new User("Luis Salm�o", 100100112));
		users.put("213546469", new User("Duarte Neves", 213546469));
		
		users.forEach((key, value) -> {
			System.out.print("key: " + key);
			System.out.println(", Value: " + value.getName() + " " + value.getNif());
		});
		
	}
	
	private static void putKeyIfNotContainsWithoutLambda() {

		System.out.println("putKeyIfNotContainsWithoutLambda");
		
		Map<String, User> users = new HashMap<>();
		
		if(!users.containsKey("213546469")) {
			users.put("213546469", new User("Maria Leal", 250720132));
		}
		
		
		users.forEach((key, value) -> {
			System.out.print("key: " + key);
			System.out.println(", Value: " + value.getName() + " " + value.getNif());
		});
		
	}

	private static void putKeyIfNotContainsWithLambda() {

		System.out.println("putKeyIfNotContainsWithLambda");
		
		Map<String, User> users = new HashMap<>();
		
		users.putIfAbsent("213546469", new User("Maria Leal", 250720132));
		
		
		users.forEach((key, value) -> {
			System.out.print("key: " + key);
			System.out.println(", Value: " + value.getName() + " " + value.getNif());
		});
		
	}
	
	private static void addCountWithoutLambda() {

		Map<String, User> users = new HashMap<>();
		
		users.put("213546469", new User("Duarte Neves", 213546469));

		System.out.println("addCountWithoutLambda Count: " + users.get("213546469").getCount());
		
		
		if(users.containsKey("213546469")) {
			
			User user = users.get("213546469");
			user.add();
			
			users.put("213546469", user);
			
		}
		
		System.out.println("addCountWithoutLambda Count: " + users.get("213546469").getCount());
		
	}	

	private static void addCountWithLambda() {
		
		Map<String, User> users = new HashMap<>();
		
		users.put("213546469", new User("Duarte Neves", 213546469));
		
		System.out.println("addCountWithLambda Count: " + users.get("213546469").getCount());
		
		
		users.computeIfPresent("213546469", (String key, User user) -> user.add());

		System.out.println("addCountWithLambda Count: " + users.get("213546469").getCount());
		
	}	
	
	/**
	 * MAIN
	 */
	public static void main(String[] args) {
		
		printKeysValuesWithoutLambda();
		
		printKeysValuesWithLambda();
		
		putKeyIfNotContainsWithoutLambda();
		
		putKeyIfNotContainsWithLambda();
		
		addCountWithoutLambda();
		
		addCountWithLambda();
		
	}
	
}

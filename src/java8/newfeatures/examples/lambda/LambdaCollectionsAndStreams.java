package java8.newfeatures.examples.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Package java.util.function 
 * 		https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html
 * 
 * Interface Iterable<T>
 * 		https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html

 * Interface Stream<T>
 * 		https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html
 * 
 */
public class LambdaCollectionsAndStreams {

	private static void forEachWithoutLambda() {

		System.out.println("[forEachWithoutLambda] Imprimir os n�meros da lista: ");

		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

		for (Integer n : numbers) {
			System.out.print(n);
		}

		System.out.println();
		
	}

	private static void forEachWithLambda() {

		System.out.println("[forEachWithLambda] Imprimir os n�meros da lista: ");

		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
		
		numbers.forEach(n -> System.out.print(n));
		
		System.out.println();

	}

	private static void orderByWithoutLambda() {

		List<User> users = Arrays.asList(
				new User("Am�rico dos Santos", 224878824),
				new User("Alexandre Safara", 123456789), 
				new User("Luis Salm�o", 100100112), 
				new User("Duarte Neves", 213546469));

		
		
		System.out.println("Ordenar utilizadores pelo nome:");

		Collections.sort(users, new Comparator<User>() {

			@Override
			public int compare(User one, User other) {
				return one.getName().compareTo(other.getName());
			}

		});

		for (User user : users) {
			System.out.println(user.getName() + " " + user.getNif());
		}
		
		
		
		System.out.println("Ordenar utilizadores pelo nif:");

		Collections.sort(users, new Comparator<User>() {

			@Override
			public int compare(User one, User other) {
				return Long.valueOf(one.getNif()).compareTo(Long.valueOf(other.getNif()));
			}

		});

		for (User user : users) {
			System.out.println(user.getName() + " " + user.getNif());
		}

		
	}

	private static void orderByLambda() {

		List<User> users = Arrays.asList(
				new User("Am�rico dos Santos", 224878824),
				new User("Alexandre Safara", 123456789), 
				new User("Luis Salm�o", 100100112), 
				new User("Duarte Neves", 213546469));

		
		System.out.println("Ordenar utilizadores pelo nome:");
		
		Collections.sort(users, 
				(User one, User other) -> one.getName().compareTo(other.getName()));
		
		users.forEach(user -> System.out.println(user.getName() + " " + user.getNif()));

		
		System.out.println("Ordenar utilizadores pelo nif:");
		
		Collections.sort(users,
				(User one, User other) -> Long.valueOf(one.getNif()).compareTo(Long.valueOf(other.getNif())));
		
		users.forEach(user -> System.out.println(user.getName() + " " + user.getNif()));

		
	}

	private static void filterWithoutLambda() {

		List<User> users = Arrays.asList(
				new User("Am�rico dos Santos", 224878824), 
				new User("Alexandre Safara", 123456789), 
				new User("Luis Salm�o", 100100112), 
				new User("Duarte Neves", 213546469));
		
		
		System.out.println("Utilizadores cujo o nome come�a por A:");

		
		long start = System.currentTimeMillis();

		List<User> result = new ArrayList<>();
		
		for (User user : users) {
			
			if("A".equalsIgnoreCase(user.getName().substring(0, 1))) {
				result.add(user);
			}
			
		}
		
		for (User user: result) {
			System.out.println(user.getName());
		}

		long end = System.currentTimeMillis();
		
		
		System.out.println("Tempo de execu��o: " + (end - start));
		
	}

	private static void filterWithLambda() {
		
		List<User> users = Arrays.asList(
				new User("Am�rico dos Santos", 224878824), 
				new User("Alexandre Safara", 123456789), 
				new User("Luis Salm�o", 100100112), 
				new User("Duarte Neves", 213546469));
		
		
		System.out.println("Utilizadores cujo o nome come�a por A:");
		
		
		long start = System.currentTimeMillis();
		
		List<User> result = 
				users.stream().filter(p -> p.getName().startsWith("A")).collect(Collectors.toList());
		
		result.forEach(user -> System.out.println(user.getName()));		
		
		long end = System.currentTimeMillis();
		
		
		System.out.println("Tempo de execu��o: " + (end - start));
		
	}
	
	
	/**
	 * MAIN
	 */
	public static void main(String[] args) {
	
		forEachWithoutLambda();
		
		forEachWithLambda();
		
		orderByWithoutLambda();
		
		orderByLambda();
		
		filterWithoutLambda();

		filterWithLambda();
		
	}

}

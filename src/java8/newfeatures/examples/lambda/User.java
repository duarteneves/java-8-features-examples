package java8.newfeatures.examples.lambda;

public class User {

	private String name;
	private long nif;
	
	private int count;

	public User(String name, long nif) {
		this.name = name;
		this.nif = nif;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getNif() {
		return nif;
	}

	public void setNif(long nif) {
		this.nif = nif;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public User add() {
		this.count =+ this.count + 1;
		return this;
	}

}
